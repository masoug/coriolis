# These variables control the compiler and linker flags. Change them as
# appropriate.

# FIRST specific stuff.
ROBOT_ADDRESS = 10.1.14.2
TARGET = FRC_UserProgram
WPILIB_INCLUDE_PATH:=../WPILib
WPILIB_SEARCH_PATH:=$(WPILIB_INCLUDE_PATH)

# List of source code.
CPP_SOURCE := $(shell find -name '*.cpp' | sed s:./:: | sed '{:q;N;s/\n/ /g;t q}')

# Everything after this line should not need to be modified for
# basic compilation. However, significant changes to the build structure
# will probably involve modifying these lines.

# Includes you'll need to include.
ADDED_INCLUDES = -I$(WPILIB_INCLUDE_PATH)

DEBUG_MODE = 1

ADDED_CFLAGS =

ifeq ($(DEBUG_MODE), 1)
OBJ_DIR := PPC603gnu_DEBUG
CFLAGS = -g -mlongcall
else
OBJ_DIR := PPC603gnu
CFLAGS = -Os -fstrength-reduce -fno-builtin -fno-strict-aliasing -mlongcall
endif

LINKFLAGS = $(CFLAGS)

# List all the *compiled* object files here, under the OBJ_DIR
# directory. Make will automatically locate the source file and
# compile it.
CPP_OBJ := $(CPP_SOURCE:%.cpp=%.o)
OBJECTS := $(foreach CPP_OBJ, $(CPP_OBJ), $(OBJ_DIR)/$(CPP_OBJ)) 

# This is the name of the output shared library.
PROJECT_TARGETS := $(OBJ_DIR)/$(TARGET).out

# If you have other VxWorks .a files to reference, list them here.
LIBS = $(WPILIB_SEARCH_PATH)/WPILib.a 
LIBPATH =

# Everything after this line should not need to be modified for
# basic compilation. However, significant changes to the build structure
# will probably involve modifying these lines.

WIND_BASE := $(subst \,/,$(WIND_BASE))

CPU = PPC603
TOOL_FAMILY = gnu
TOOL = gnu
CC_ARCH_SPEC = -mcpu=603 -mstrict-align -mno-implicit-fp

IDE_INCLUDES = -I$(WIND_BASE)/target/h -I$(WIND_BASE)/target/h/wrn/coreip 

# This basic rule compiles a .c file into a .o file. It can be adapted to
# all other source files that gcc can compile, including assembly (.s) and
# C++ (.cpp, .cc, .C, .cxx) files. To enable support for those extensions,
# copy this rule and modify its extension and compile flags for the
# required source file type.
$(OBJ_DIR)/%.o : %.c
	@ccppc $(CFLAGS) $(CC_ARCH_SPEC) -ansi  -Wall  -MD -MP $(ADDED_CFLAGS) $(IDE_INCLUDES) $(ADDED_INCLUDES) -DCPU=$(CPU) -DTOOL_FAMILY=$(TOOL_FAMILY) -DTOOL=$(TOOL) -D_WRS_KERNEL   $(DEFINES) -o "$@" -c "$<"

# Adapted rule for .cpp files
$(OBJ_DIR)/%.o: %.cpp
	@echo "Compiling $<"
	@c++ppc $(CFLAGS) $(CC_ARCH_SPEC) -ansi  -Wall  -MD -MP $(ADDED_CFLAGS) $(IDE_INCLUDES) $(ADDED_INCLUDES) -DCPU=$(CPU) -DTOOL_FAMILY=$(TOOL_FAMILY) -DTOOL=$(TOOL) -D_WRS_KERNEL $(DEFINES) -o "$@" -c "$<"

all : check_objectdir $(PROJECT_TARGETS) 

$(PROJECT_TARGETS) : $(OBJECTS)
	@echo "Munching and linking $(OBJECTS)"
	@rm -f "$@" ctdt.c;nmppc $(OBJECTS) | tclsh $(WIND_BASE)/host/resource/hutils/tcl/munch.tcl -c ppc > ctdt.c
	@ccppc $(LINKFLAGS) $(CC_ARCH_SPEC) -fdollars-in-identifiers -Wall $(ADDED_CFLAGS) $(IDE_INCLUDES) $(ADDED_INCLUDES) -DCPU=$(CPU) -DTOOL_FAMILY=$(TOOL_FAMILY) -DTOOL=$(TOOL) -D_WRS_KERNEL   $(DEFINES)  -o ctdt.o -c ctdt.c
	@ccppc -r -nostdlib -Wl,-X -T $(WIND_BASE)/target/h/tool/gnu/ldscripts/link.OUT -o "$@" $(OBJECTS) $(LIBPATH) $(LIBS)  $(ADDED_LIBPATH) $(ADDED_LIBS) ctdt.o
	@rm -f ctdt.c ctdt.o
	@echo "Compilation successfully completed."


check_objectdir :
	@if [ ! -d "$(OBJ_DIR)" ]; then\
		mkdir -p $(OBJ_DIR);\
	fi

clean :
	@echo "Cleaning workspace..."
	@rm -f $(OBJECTS) $(PROJECT_TARGETS) $(wildcard $(OBJ_DIR)/*.unstripped)
	@rm -f $(OBJ_DIR)/*.o $(OBJ_DIR)/*.d $(OBJ_DIR)/Modules/*.o $(OBJ_DIR)/Modules/*.d
	@rm -rf html

.PHONY: deploy reboot doc style
deploy: check_objectdir $(PROJECT_TARGETS)
	@echo "Deploying to robot..."
	@wput $(PROJECT_TARGETS) ftp://anonymous@$(ROBOT_ADDRESS)/ni-rt/system/FRC_UserProgram.out

reboot:
	@echo "Rebooting robot..."
	@echo "reboot" | nc -u -v $(ROBOT_ADDRESS) 7654

doc:
	@echo "Generating documentation..."
	@doxygen

style:
	@echo "Styling source code..."
	@astyle --style=ansi --mode=c -r $(CPP_SOURCE)
	@echo "Removing originals..."
	@rm -rf *.orig
	@rm -rf Modules/*.orig

.DUMMY: check_objectdir clean
