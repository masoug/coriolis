#include "WPILib.h"
#include "Config.h"
#include "Modules/DriveBase.h"
#include "Modules/Log.h"
#include "Modules/Rebooter.h"

/**
 * This is the SimpleRobot implementation for the 2012 FRC game (Rebound Rumble).
 * It implements the minor and major components for the robot.
 *
 * @brief Main robot class.
 * @author Sammy Guo
 */
class Coriolis : public SimpleRobot, public Log
{
public:
    Coriolis(void);
    void RobotMain(void);
    void Autonomous(void);

    /* Robot-specific commands. */
    void Fire();

private:
Task autoTask;
    DriveBase base;
    Joystick leftJoystick;
    Joystick rightJoystick;
    Rebooter bootControl;
    Compressor compressor;
};

static void spawn(Coriolis *ptr)
{
ptr->Write("spawn() Autonomous thread.");
ptr->Autonomous();
}

Coriolis::Coriolis() :
    Log("Coriolis"), autoTask("CoriolisAuto", (FUNCPTR)spawn),
    base(new RobotDrive((unsigned int)LEFT_DRIVE_PORT, (unsigned int)RIGHT_DRIVE_PORT)),
    leftJoystick(LEFT_JOYSTICK), rightJoystick(RIGHT_JOYSTICK),
    compressor(COMPRESSOR_PRESSURE, COMPRESSOR_RELAY)
{
    /* Echo debug data. */
    Echo();

    /* Setup robot components. */
    /*base.SetEncoder(new Encoder((unsigned int)LEFT_DRIVE_ENCODER_A, (unsigned int)LEFT_DRIVE_ENCODER_B),
                    new Encoder((unsigned int)RIGHT_DRIVE_ENCODER_A, (unsigned int)RIGHT_DRIVE_ENCODER_B));
    base.SetSolenoid(new Solenoid(HIGH_GEAR_SOLENOID),
                     new Solenoid(LOW_GEAR_SOLENOID));*/
    compressor.Start();
}

void Coriolis::RobotMain()
{
    Write("Coriolis start.");
    int counter = 0;

    while (true)
    {
        if (IsAutonomous())
        {
            if (IsEnabled() && !autoTask.Verify())
            {
                Write("Entering autonomous mode.");
                ASSERT(autoTask.Start((INT32)this));
            }
            else if (IsDisabled() && autoTask.Verify())
            {
                Write("Automous stop.");
                ASSERT(autoTask.Stop());
            }
        }
        else if (IsOperatorControl())
        {
            /* Teleop. */
            base.Manual();
            base.Set(leftJoystick.GetY(), rightJoystick.GetY());
            base.Gear(leftJoystick.GetTrigger());
        }
        else
        {
            /* Disabled. */
            base.Reset();
        }

        /* Status light. */
        if (counter % 250 == 0)
            ToggleRIOUserLED();
        counter++;
    }
}

void Coriolis::Autonomous()
{
    /* Auto routine. */
}

START_ROBOT_CLASS(Coriolis);

