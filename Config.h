#ifndef CONFIG_H
#define CONFIG_H

/* Conversion macros for velocity. */
#define TOIMP(m) 2.23693629*m
#define TOMET(i) 0.44704*i

/* Specific ports to use for the robot. */
#define LEFT_DRIVE_PORT 0
#define RIGHT_DRIVE_PORT 0
#define LEFT_DRIVE_ENCODER_A 0
#define LEFT_DRIVE_ENCODER_B 0
#define RIGHT_DRIVE_ENCODER_A 0
#define RIGHT_DRIVE_ENCODER_B 0
#define DRIVE_GYRO 0
#define HIGH_GEAR_SOLENOID 0
#define LOW_GEAR_SOLENOID 0
#define LEFT_JOYSTICK 0
#define RIGHT_JOYSTICK 0
#define BRIDGE_UP_SOLENOID 0
#define BRIDGE_DOWN_SOLENOID 0
#define COMPRESSOR_PRESSURE 0
#define COMPRESSOR_RELAY 0

#endif
