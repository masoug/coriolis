#ifndef LOG_H
#define LOG_H

#include "stdio.h"
#include "time.h"
#include <string>

/* This can only be called within an extended class. */
#define ASSERT(expression) Assert(expression, #expression, __FILE__, __LINE__, __FUNCTION__)

/*
 * Logging base that allows for data logging and error messaging.
 */
class Log
{
public:
    Log(const std::string name);
    void Write(const std::string fmt, ...);
    void Echo(bool echo = true);
    void Assert(bool exp, const char *expName, const char *file,
                unsigned int line, const char *func);
    ~Log();

private:
    FILE *m_fp;
    std::string m_name;
    time_t m_rawtime;
    bool m_echo;

};

#endif
