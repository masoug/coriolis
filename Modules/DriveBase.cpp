#include "DriveBase.h"

static void spawn(DriveBase *ptr)
{
    ptr->Write("spawn() Control thread.");
    ptr->Control();
}

/*
 * Main constructor instance, where the base is initialized, while starting the control loop.
 */
DriveBase::DriveBase(RobotDrive *drive) :
    Log("DriveBase"), m_control("DriveBaseControl", (FUNCPTR)spawn),
    m_leftVelocity(0.0), m_rightVelocity(0.0), m_auto(false), m_highGear(false)
{
    Echo();

    // Drivebase initialization here.
    m_semaphore = semMCreate(SEM_Q_PRIORITY |
                             SEM_INVERSION_SAFE | SEM_DELETE_SAFE);

    /* Setup hardware. */
    ASSERT(drive); // Must have a drivebase.
    m_drive = drive;
    m_leftEncoder = NULL;
    m_rightEncoder = NULL;
    m_gyro = NULL;
    m_solenoidHighGear = NULL;
    m_solenoidLowGear = NULL;

    /* Spawn control task. */
    ASSERT(m_control.Start((INT32)this));
}

void DriveBase::SetEncoder(Encoder *leftEncoder, Encoder *rightEncoder)
{
    CRITICAL_REGION(m_semaphore)
    m_leftEncoder = leftEncoder;
    m_rightEncoder = rightEncoder;
    END_REGION
}

void DriveBase::SetGyro(Gyro *gyro)
{
    CRITICAL_REGION(m_semaphore)
    m_gyro = gyro;
    END_REGION
}

void DriveBase::SetSolenoid(Solenoid *highGear, Solenoid *lowGear)
{
    CRITICAL_REGION(m_semaphore)
    m_solenoidHighGear = highGear;
    m_solenoidLowGear = lowGear;
    END_REGION
}

/* "Control Panel" */
void DriveBase::Set(float leftVelocity, float rightVelocity)
{
    CRITICAL_REGION(m_semaphore)
    m_leftVelocity = leftVelocity;
    m_rightVelocity = rightVelocity;
    END_REGION
}

void DriveBase::Go(float dist)
{
    if (m_leftEncoder && m_rightEncoder)
    {
        CRITICAL_REGION(m_semaphore)
        if (!m_auto)
            m_auto = true;
        /* Set flags here... */
        END_REGION
    }
    else
        return;
}

void DriveBase::Turn(float degrees)
{
    CRITICAL_REGION(m_semaphore)
    if (!m_auto)
        m_auto = true;
    END_REGION
}

float DriveBase::Heading()
{
    float result;
    CRITICAL_REGION(m_semaphore)
    if (m_gyro)
        result = m_gyro->GetAngle();
    else
        result = 0;
    END_REGION
    return result;
}

void DriveBase::Auto()
{
    CRITICAL_REGION(m_semaphore)
    m_auto = true;
    END_REGION
}

void DriveBase::Manual()
{
    CRITICAL_REGION(m_semaphore)
    m_auto = false;
    END_REGION
}

bool DriveBase::IsAuto()
{
    bool flag;
    CRITICAL_REGION(m_semaphore)
    flag = m_auto;
    END_REGION
    return flag;
}

void DriveBase::Reset()
{
    if (m_leftEncoder && m_rightEncoder && m_gyro)
    {
        CRITICAL_REGION(m_semaphore)
        m_leftEncoder->Reset();
        m_rightEncoder->Reset();
        m_gyro->Reset();
        END_REGION
    }
}

void DriveBase::Gear(bool high)
{
    CRITICAL_REGION(m_semaphore)
    m_highGear = high;
    END_REGION
}

bool DriveBase::IsHighGear()
{
    bool result;
    CRITICAL_REGION(m_semaphore)
    result = m_highGear;
    END_REGION
    return result;
}

/* Main control loop function. */
void DriveBase::Control()
{
    /* Using control loops, set the velocity of the drive
     * wheels based on the encoder counts.
     */
    while (true)
    {
        CRITICAL_REGION(m_semaphore)
        if (m_auto)
        {
            /* Control-loop based stuff. */
        }
        else /* Manual control. */
            m_drive->TankDrive(m_leftVelocity, m_rightVelocity);

        /* Handle gearing. */
        if (m_solenoidHighGear && m_solenoidLowGear) // Must have solenoid objects
        {
            if (m_highGear)
            {
                m_solenoidHighGear->Set(true);
                m_solenoidLowGear->Set(false);
            }
            else
            {
                m_solenoidHighGear->Set(false);
                m_solenoidLowGear->Set(true);
            }
        }
        END_REGION
    }
}
