#ifndef UDP_SOCKET_H_
#define UDP_SOCKET_H_

/* This is the version of Socklib that is compatible with vxworks. */

#include "types.h"
#include "sockLib.h"
#include "inetLib.h"
#include "stdlib.h"

class UdpSocket
{
public:
    UdpSocket(unsigned int bufferSize);
    bool Bind(const char *addr, int port);
    int Receive();
    int Send();
    void ClearBuffer();
    char* GetBuffer() const;
    ~UdpSocket();

private:
    int m_sock;
    unsigned int m_bufSize;
    sockaddr_in m_fromAddr; /* Remote address. */
    char *m_buffer;
};


#endif
