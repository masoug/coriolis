#include "Log.h"
#include "stdarg.h"
#include "string.h"
#include "stdlib.h"
#include "stdio.h"
#include <sstream>

Log::Log(const std::string name)
{
    m_fp = fopen(name.c_str(), "a");
    if (!m_fp)
        perror("%s: Failed to open file.");
    m_name = name;
    time(&m_rawtime);
    struct tm *timeinfo = localtime(&m_rawtime);
    fprintf(m_fp, "\n%s: Initialization. %s", m_name.c_str(), asctime(timeinfo));
}

void Log::Write(const std::string fmt, ...)
{
    time_t now;
    time(&now);
    std::stringstream msg;
    msg << m_name << ": [ " << (int)difftime(now, m_rawtime) << " ] " << fmt << std::endl;
    /* Start variable arugment list. */
    va_list arguments;
    va_start(arguments, fmt);
    /* Write message. */
    vfprintf(m_fp, msg.str().c_str(), arguments);
    if(m_echo)
        vprintf(msg.str().c_str(), arguments);
    va_end(arguments);
}

void Log::Echo(bool echo)
{
    m_echo = echo;
}

void Log::Assert(bool exp, const char *expName, const char *file,
                 unsigned int line, const char *func)
{
    if (!exp)
    {
        Write("FATAL ERROR: Assertion failed for %s in %s at line %d within %s. Aborting.\n",
              expName, func, line, file);
        abort();
    }
}

Log::~Log()
{
    fclose(m_fp);
}
