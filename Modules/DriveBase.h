#ifndef DRIVE_BASE_H
#define DRIVE_BASE_H

#include "WPILib.h"
#include "Log.h"

class DriveBase : public Log
{
public:
    /* Setup functions. */
    DriveBase(RobotDrive *drive);
    void SetEncoder(Encoder *leftEncoder, Encoder *rightEncoder);
    void SetGyro(Gyro *gyro);
    void SetSolenoid(Solenoid *highGear, Solenoid *lowGear);

    /* "Control Panel" */
    void Set(float leftVelocity, float rightVelocity);
    void Go(float dist);
    void Turn(float degrees);
    float Heading();
    void Auto();
    void Manual();
    bool IsAuto();
    void Reset();
    void Gear(bool high);
    bool IsHighGear();

    /* Main control loop function. */
    void Control();

private:
    Task m_control;
    SEM_ID m_semaphore;

    /* Robot hardware. */
    RobotDrive *m_drive;
    Encoder *m_leftEncoder;
    Encoder *m_rightEncoder;
    Gyro *m_gyro;
    Solenoid *m_solenoidHighGear;
    Solenoid *m_solenoidLowGear;

    /* Control flags. */
    float m_leftVelocity, m_rightVelocity;
    bool m_auto, m_highGear;
};

#endif
