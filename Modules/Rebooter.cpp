#include "Rebooter.h"

static void spawnServer(Rebooter *ptr)
{
    ptr->Server();
}

Rebooter::Rebooter() :
    Log("bootCtrl"),
    m_server("bootCtrl", (FUNCPTR)spawnServer),
    m_socket(16)
{
    Echo();
    ASSERT(m_socket.Bind("10.1.14.2", 7654));
    ASSERT(m_server.Start((INT32)this));
}

void Rebooter::Server()
{
    /* Start main service loop. */
    while (true)
    {
        m_socket.Receive();
        if (strcmp(m_socket.GetBuffer(), "reboot") == 0)
            Reboot();
        else
        {
            Write("Recieved non-reboot message.");
            Write("WARNING: There should not be any traffic on port 7654.");
        }
        m_socket.ClearBuffer();
    }
}

void Rebooter::Reboot()
{
    Write("Reboot now!");
    reboot(BOOT_NORMAL);
}
