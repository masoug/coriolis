#ifndef REBOOTER_H
#define REBOOTER_H

#include "WPILib.h"
#include "UdpSocket.h"
#include "Log.h"
#include "rebootLib.h"
#include "sysLib.h"

/* A rebooting object that reboots the
 * cRIO based on a socket signal.
 */
class Rebooter : public Log
{
public:
    Rebooter();
    void Server();
    void Reboot();

private:
    Task m_server;
    UdpSocket m_socket;
};

#endif
